import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.net.URL;

import com.google.gson.*;

public class Wxhw
{
	public String getWx(String zip)
	{
		JsonElement jse = null;
    String wxReport = null;

		try
		{
			// Construct WxStation API URL
			URL wxURL = new URL("http://api.wunderground.com/api/4cf11bd878689f74/conditions/q/"
					+ zip
					+ ".json");
			// Open the URL
			InputStream is = wxURL.openStream(); // throws an IOException
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			// Read the result into a JSON Element
			jse = new JsonParser().parse(br);
      
			// Close the connection
			is.close();
			br.close();
		}
		catch (java.io.UnsupportedEncodingException uee)
		{
			uee.printStackTrace();
		}
		catch (java.net.MalformedURLException mue)
		{
			mue.printStackTrace();
		}
		catch (java.io.IOException ioe)
		{
			ioe.printStackTrace();
		}

		if (jse != null)
		{
      // Build a weather report
	  String full = jse.getAsJsonObject().get("observation_location")
                       .getAsJsonObject().get("full").getAsString();
      wxReport = wxReport + "Location: " + full + "\n";

      String time = jse.getAsJsonObject().get("observation_time").getAsString();
      wxReport = wxReport + "Time: " + time + "\n";

      String weather = jse.getAsJsonObject().get("weather").getAsString();
      wxReport = wxReport + "Weather: " + weather + "\n";
      
      String temp = jse.getAsJsonObject().get("temp_f").getAsString();
      wxReport = wxReport + "Temperature F: " + temp + "\n";
      
      String wind_dir = jse.getAsJsonObject().get("wind_dir").getAsString();
      String wind_mph = jse.getAsJsonObject().get("wind_mph").getAsString();
      String wind_gmph = jse.getAsJsonObject().get("wind_gust_mph").getAsString();
      wxReport = wxReport + "Wind: From the" + wind_dir + " at " + wind_mph + " Gusting to " + wind_gmph + "\n";

      String pressure = jse.getAsJsonObject().get("pressure_in").getAsString();
      wxReport = wxReport + "Temperature F: " + pressure + "\n";
		}
    return wxReport;
	}

	public static void main(String[] args)
	{
		Wxhw b = new Wxhw();
    if ( args.length == 0 )
      System.out.println("Please enter a zip code as the first argument.");
    else
    {
		  String wx = b.getWx(args[0]);
      if ( wx != null )
		    System.out.println(wx);
    }
	}
}
